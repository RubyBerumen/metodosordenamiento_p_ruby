'''
Created on 23 nov. 2020

@author: Ruby
'''

class MetodosOrdenamiento:
    def __init__(self):
        pass
    
    class Burbuja:
        
        def ordenacionBurbuja1(self, numeros):
            for i in range (1,len(numeros)):
                for j in range (len(numeros)-i):
                    if(numeros[j]>numeros[j+1]):
                        aux = numeros[j]
                        numeros[j]=numeros[j+1]
                        numeros[j+1]=aux
                    
        def ordenacionBurbuja2(self, numeros):
            i=1
            ordenado=False
            while (i<len(numeros)):
                ordenado=True
                for j in range(len(numeros)-i):
                    if(numeros[j]>numeros[j+1]):
                        ordenado=False
                        aux = numeros[j]
                        numeros[j]=numeros[j+1]
                        numeros[j+1]=aux
                i+=1
    
        def ordenacionBurbuja3(self, numeros):
            i=1
            ordenado=True
            for j in range(len(numeros)-1):             
                if(numeros[j]>numeros[j+1]):
                    ordenado=False
                    aux = numeros[j]
                    numeros[j]=numeros[j+1]
                    numeros[j+1]=aux
            i+=1
            while (i<len(numeros)):
                ordenado=True
                for j in range(len(numeros)-i):
                    if(numeros[j]>numeros[j+1]):
                        ordenado=False
                        aux = numeros[j]
                        numeros[j]=numeros[j+1]
                        numeros[j+1]=aux
                i+=1    


    class Insercion:
    
        def ordenarInsercion(self, numeros):
            aux=0
        
            for i in range (1,len(numeros)):
                aux=numeros[i]
            
                j=(i-1)
                while(j>=0 and numeros[j]>aux):
                    numeros[j+1]=numeros[j]
                    numeros[j]=aux
                    j-=1


    class OrdenamientoPorSeleccion:
        
        def ordenamientoSeleccion(self, numeros):
            for i in range(len(numeros)):
                for j in range(i,len(numeros)):
                    if (numeros[i] > numeros[j]):
                        minimo = numeros[i]
                        numeros[i]=numeros[j]
                        numeros[j]=minimo
                    

    class Quicksort:
        
        def ordenar(self, numeros, izq, der):
            pivote = numeros[izq]
            i = izq
            j = der
            aux = 0
        
            while i < j:
                while numeros[i] <= pivote and i<j:
                    i+=1
                    while numeros[j] > pivote:
                        j-=1
                    if i < j:
                        aux = numeros[i]
                        numeros[i] = numeros[j]
                        numeros[j] = aux
                
            numeros[izq] = numeros[j]
            numeros[j] = pivote
        
            if izq < j-1:
                self.ordenar(numeros, izq, j-1)
            if j+1 < der:
                self.ordenar(numeros, j+1, der)
            
                
    class Shellsort:
    
        def ordenar(self,numeros):
            intervalo = len(numeros)/2
            intervalo = int(intervalo)
            while(intervalo>0):
                for i in range(int(intervalo),len(numeros)):
                    j=i-int(intervalo)
                    while(j>=0):
                        k=j+int(intervalo)
                        if(numeros[j] <= numeros[k]):
                            j-=1
                        else:
                            aux=numeros[j]
                            numeros[j]=numeros[k]
                            numeros[k]=aux
                            j-=int(intervalo)
            
                intervalo=int(intervalo)/2
     
     
    class Radix:                

        def countingSort(self, arr, exp1):
            n = len(arr)
            output = [0] * (n)
            count = [0] * (10)

            for i in range(0, n):
                index = (arr[i]/exp1)
                count[int((index)%10)] += 1
            for i in range(1,10):
                count[i] += count[i-1]
            i = n-1
            while i>=0:
                index = (arr[i]/exp1)
                output[ count[ int((index)%10) ] - 1] = arr[i]
                count[int((index)%10)] -= 1
                i -= 1

            i = 0
            for i in range(0,len(arr)):
                arr[i] = output[i]

        def radixSort(self, arr, mo):
            max1 = max(arr)
            exp = 1
            while max1/exp > 0:
                mo.Radix.countingSort(mo,arr,exp)
                exp *= 10
                
            
    def mostrarVector(self,numeros):
        print(f"Vector ordenado: {numeros} \n\n")


mo = MetodosOrdenamiento()

while(True):
    numeros = [6,2,9,7,1,0,4]            
    print(f"Vector desordenado: {numeros}")
    print("Ordenar el arreglo por metodo de:")
    print("1. Burbuja");
    print("2. Insercion");
    print("3. Seleccion ");
    print("4. Quicksort");
    print("5. Shellsort");
    print("6. Radix");
    print("7. Salir");
    op = int(input("Escribe una de las opciones\n"))
    
    if(op==1):
        while(True):
                
            print("Ordenar el arreglo por metodo de:")
            print("1. Burbuja1");
            print("2. Burbuja2");
            print("3. Burbuja3");
            print("4. Salir");
            opc = int(input("Escribe una de las opciones\n"))
            
            if(opc==1):
                mo.Burbuja.ordenacionBurbuja1(mo,numeros)
                print(f"Vector ordenado: {numeros} \n\n")
            elif(opc==2):
                mo.Burbuja.ordenacionBurbuja2(mo,numeros)
                print(f"Vector ordenado: {numeros} \n\n")
            elif(opc==3):
                mo.Burbuja.ordenacionBurbuja3(mo,numeros)
                print(f"Vector ordenado: {numeros} \n\n")
            elif(opc==4):
                break
            else:
                print("Debes ingresar numeros entre 1 y 4 ")
        
    elif(op==2):
        mo.Insercion.ordenarInsercion(mo,numeros)
        print(f"Vector ordenado: {numeros} \n\n")
        
    elif(op==3):
        mo.OrdenamientoPorSeleccion.ordenamientoSeleccion(mo,numeros)
        print(f"Vector ordenado: {numeros} \n\n")
        
    elif(op==4):
        mo.Quicksort.ordenar(mo,numeros,0,len(numeros)-1)
        print(f"Vector ordenado: {numeros} \n\n")
        
    elif(op==5):
        mo.Shellsort.ordenar(mo,numeros)
        print(f"Vector ordenado: {numeros} \n\n")
        
    elif(op==6):
        
        mo.Radix.radixSort(mo,numeros,mo)
        print(f"Vector ordenado: {numeros} \n\n")
        
    elif(op==7):
        break
    
    else:
        print("Debes ingresar numeros entre 1 y 7 ")
